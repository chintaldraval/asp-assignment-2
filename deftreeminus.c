// For std in/out commands
#include<stdio.h>
// bool not an inbuilt type, this header allows integrating it
#include<stdbool.h>
// for exit cmnds
#include<stdlib.h>
// comparing strs and chrs
#include<string.h>
// for SIGKILL communication
#include<sys/signal.h>


// Basic info for integrating asked functionality
bool ex_id_provided = false;
int ex_id;
bool option_provided = false;
char *chosen_option;
int passed_t_of_process;
int def_nos;
int root_id;
int tree[1000];
int count_p_in_tree = 0;

// Called when a mistake is made
void print_synopsis(){
    printf("\nSynopsis: deftreeminus [OPTION1] [OPTION2] [-processid]\n");
    printf("Option 1: ");
    printf("-t or -b\n");
    printf("Option 2: ");
    printf("PROC_ELTIME or NO_OF_DFCS\n");

}

// Called everytime for communicating error msgs requiring involvement of args
void throw_arg_err(){
    fprintf(stderr, "Please check the arguments.");
    print_synopsis();
    exit(1);
}

void check_arguments_fill_data(int *argc, char *argv[]){
    if(*argc < 2){
        throw_arg_err();    
    }
    else if(*argc == 3){
        char *last_el_pt = argv[*argc - 1];
        // Args are 3, but need to check if last arg is proper
        char last_el[4];
        strncpy(last_el, last_el_pt, 2);
        if((strcmp(argv[*argc - 1], "-t") == 0) || (strcmp(argv[*argc - 1], "-b") == 0)){
            // not valid
            throw_arg_err();
        }
        if(last_el[0] == '-'){
            // last arg valid
            ex_id_provided = true;
            ex_id = atoi(argv[*argc - 1] + 1);
            if(ex_id == 0){
                // last arg type not good
                throw_arg_err();
            }
        }
        else{
            // no other scene is acceptable for 3 args
            throw_arg_err();
        }
    }
    else if(*argc == 4){
        char *last_el_pt = argv[*argc - 1];
        // args 4, check for last arg
        char last_el[1];
        strncpy(last_el, last_el_pt, 1);
        if(last_el[0] == '-'){
            // not okay
            throw_arg_err();
        }
        if(strcmp(argv[*argc - 2], "-t") == 0){
            chosen_option = "-t";
            passed_t_of_process = atoi(argv[*argc -1]);
            option_provided = true;
        }
        else if(strcmp(argv[*argc - 2], "-b") == 0){
            chosen_option = "-b";
            def_nos = atoi(argv[*argc - 1]);
            option_provided = true;
        }
        else{
            // not an acceptable scenario 
            throw_arg_err();
        }
    }
    else if(*argc == 5){
        char *last_el_pt = argv[*argc - 1];
        // validate last arg
        char last_el[1];
        strncpy(last_el, last_el_pt, 1);
        if(last_el[0] != '-'){
            throw_arg_err();
        }
        if(strcmp(argv[*argc - 3], "-t") == 0){
            chosen_option = "-t";
            passed_t_of_process = atoi(argv[*argc - 2]);
            option_provided = true;
        }
        else if(strcmp(argv[*argc - 3], "-b") == 0){
            chosen_option = "-b";
            def_nos = atoi(argv[*argc - 2]);
            option_provided = true;
        }
        else{
            // not a valid scenario, throw err
            throw_arg_err();
        }
        ex_id_provided = true;
        // + 1 means get the next element
        ex_id = atoi(argv[*argc - 1]+1);
        if(ex_id == 0){
            // type of excluding id not valid
            throw_arg_err();
        }
    }
    else if(*argc == 2){
        root_id = atoi(argv[1]);
        if(root_id == 0){
            throw_arg_err();
        }
    }
    else{
        throw_arg_err();
    }
    root_id = atoi(argv[1]);
    if(root_id == 0){
            throw_arg_err();
    }
}

// a dedicated fn for transforming and finding a particular chr
char* get_bracket(int value, char *ln) {
    if (value == 1) {
        // 1 is received as an opt
        return strchr(ln, '(');
    } else if (value == 2) {
        // got 2 as option
        return strchr(ln, ')');
    }
}

void fill_ids_from_tree(){
    FILE *process_tree_fl = fopen("deftree.txt", "r");
    if (process_tree_fl == NULL) {
        fprintf(stderr, "Failed to open the file");
        exit(1);
    }
    // chr arr for our txt
    char ln[1001];  
    // info is in txt as rows, go over each of them
     while (fgets(ln, sizeof(ln), process_tree_fl) != NULL) {
        // vars for wokring with info
        char* leftBrk;
        char* rtBrk;
        int process_id;
        char* line = ln;
        // a row can have more than 1 id, go over whole ln
        while ((leftBrk = get_bracket(1, line)) != NULL && (rtBrk = get_bracket(2, line)) != NULL) {
            process_id = atoi(leftBrk + 1);
            if (process_id != 0) {
                // valid info, fill arr
                tree[count_p_in_tree] = process_id;
                count_p_in_tree++;
            }
            line = rtBrk + 1;
        }
    }
    fclose(process_tree_fl);
    // no need of txt fl anymore
    remove("deftree.txt");
}

void get_process_tree(char *argv[]){
    char cli[50];
    // same as above, init arr and get info 
    int id_of_root_process = atoi(argv[1]);
    // required for bringing dynamic info inside arr
    sprintf(cli, "pstree -p %d > deftree.txt", id_of_root_process);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to get the process tree.\n");
        exit(1);
    }
    fill_ids_from_tree();
}

// fn that checks if found ps can be sent SIGKILL 
bool can_id_be_killed(int id, int t, int c_of_zos){
    char cli[50];
    // find if parent of ps a terminal
    sprintf(cli, "ps -o comm= -p %d > is_bash.txt", id);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to run the command.\n");
        exit(1);
    }
    FILE *bash_f = fopen("is_bash.txt", "r");
    if(bash_f == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    char ln_for_checking_bash[100];
    // similar as before, data in rows, find the right info by loop
    while(fgets(ln_for_checking_bash, sizeof(ln_for_checking_bash), bash_f) != NULL){
        char command_name_ret[15];
        // get needed type of data with below command
        sscanf(ln_for_checking_bash, "%s", command_name_ret);
        if(strcmp(command_name_ret, "bash") == 0){
            // found parent as bash
            remove("is_bash.txt");
            return false;
        }
    }
    fclose(bash_f);
    // rm not required txt files
    remove("is_bash.txt");

    if(!option_provided){
        return true;
    }
    else{
        if(strcmp(chosen_option, "-t") == 0){
            t = t/60;
            // Logic for the required assignment
            if(t > passed_t_of_process){
                return true;
            }
            else{
                return false;
            }
        } else{
            // c_of_zos is for counting no. of def PSs
            if(c_of_zos >= def_nos){
                return true;
            }
        }
    }
    return false;
}

int count_zoms(){
    FILE *f_id_for_zom = fopen("kill.txt", "r");
    if(f_id_for_zom == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    char ln_for_zom[10000];
    // info for PSs in rows: need to iterate
    int count_of_zom_for_id = 0;
    while(fgets(ln_for_zom, sizeof(ln_for_zom), f_id_for_zom) != NULL){
        int curre_id;
        char status[15];
        // for getting Z status
        int parent_of_curre_id;
        int el_t_of_p;
        // time of PS
        sscanf(ln_for_zom, "%d %s %d %d", &curre_id, status, &parent_of_curre_id, &el_t_of_p);
        if(strcmp(status, "Z") == 0){
            count_of_zom_for_id++;
        }
    }
    fclose(f_id_for_zom);
    return count_of_zom_for_id;
}

// funct that finds and communicates SIGKILL 
void get_and_kill_parents_with_def_chld(int id){
    char cli[50];
    // populate dynamic info for the chr arr
    sprintf(cli, "ps -o pid,state,ppid,etimes --no-headers --ppid %d > kill.txt", id);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to run the command.\n");
        exit(1);
    }
    FILE *id_f = fopen("kill.txt", "r");
    // open fl contatining info of PSs to potentially terminate
    if(id_f == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    int count_of_zom_for_id = count_zoms();
    char ln[10000];
    // info in rows, need chr arr to store each ln and iterate
    int id_killed = 0;
    while(fgets(ln, sizeof(ln), id_f) != NULL){
        int curre_id;
        char status[15];
        int parent_of_curre_id;
        int el_t_of_p;
        // Child/desc. of found PS, get info using this cmnd
        sscanf(ln, "%d %s %d %d", &curre_id, status, &parent_of_curre_id, &el_t_of_p);
        if(strcmp(status, "Z") == 0){
            // Same parent can be there for mul chlds, no req. to send same sig again
            if(id_killed != parent_of_curre_id){
                // Id to be excluded or not
                if(!ex_id_provided){
                    // Based on options, validate parent is bash or not 
                    bool kill_ =can_id_be_killed(parent_of_curre_id, el_t_of_p, count_of_zom_for_id);
                    if(kill_){
                        // safe to kill
                        printf("Killing: %d\n", parent_of_curre_id);
                        kill(parent_of_curre_id, SIGKILL);
                        id_killed = parent_of_curre_id;
                    }
                }
                else{
                    if(ex_id != parent_of_curre_id){
                        // Based on options, validate parent is bash or not
                        bool kill_ =can_id_be_killed(parent_of_curre_id, el_t_of_p, count_of_zom_for_id);
                        if(kill_){
                            // safe to send SIGKILL
                            printf("Killing: %d\n", parent_of_curre_id);
                            kill(parent_of_curre_id, SIGKILL);
                            id_killed = parent_of_curre_id;
                        }
                    }
                }
            }
            else{
                // PS do not match criteria, recursion is called
                get_and_kill_parents_with_def_chld(curre_id);
            }
        } 
        else{
            // PS do not match criteria, recursion is called
            get_and_kill_parents_with_def_chld(curre_id);
        }
    }
    fclose(id_f);
}

void kill_ps(){
    get_and_kill_parents_with_def_chld(root_id);
}

// program enters execution from here
int main(int argc, char *argv[]){
    check_arguments_fill_data(&argc, argv);
    get_process_tree(argv);
    kill_ps();
    return 0;
}