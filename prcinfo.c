// for printing and other standars i/o operations
#include<stdio.h>
// To integrate bool type
#include<stdbool.h>
#include<stdlib.h>

// For matching char arr and strings
#include<string.h>
// Reading files
#include<fcntl.h>


// Char and int arrs for storing different processes for different options.
// Note: Not all arrs will be filled, they are just initialized, they will be filled according to chosen option
char *chosen_option = NULL;
int process_id_arr[100];
int count_of_prc_in_file = 0;
int provided_process_ids_arr[5];
int provided_process_ids;
int non_direct_descendants_arr[10000];
int count_for_nds = 0;
int direct_descendants_arr[1000];
int count_for_ds = 0;
int sib_ids[50];
int no_sib_ids = 0;
int zom_sibs[25];
int no_zom_sibs = 0;
bool grd_child_ret = false;
int no_grd_chld = 0;
int grd_chld_arr[15];
bool id1_is_zom = false;
int dd_zz_arr[15];
int no_dd_zz = 0;


// When error arises, this function will be called.
void print_synopsis(){
    printf("\nSynopsis: prcinfo [root_process] [process_id1] [process_id2]...[process_id(n)] [OPTION]\n");
    printf("You need to provide at least one process id. At max you can provide 5.\n");
    printf("\nYou can choose one of the following options:\n\n");
    printf("-nd: display PIDs for every non-direct descendants of first PS\n");
    printf("-dd: display process ids for every child of first PS\n");
    printf("-sb: display process ids for every sibling processes of first PS\n");
    printf("-sz: display process ids for every sibling processes of first PS which became zombies.\n");
    printf("-gc: display prcoess ids for every grand child  of first PS\n");
    printf("-zz: display stat of first PS. State can be Not Defunct/ Def)\n");
    printf("-zc: display process ids in def state and which is a child of first process.\n");
}

// Verify if the last arg is an opt or not
void check_and_assign_last_element(char *last_element){
    if(strcmp(last_element, "-nd") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-dd") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-sb") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-sz") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-gc") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-zz") == 0){
        chosen_option = last_element;
    }
    else if(strcmp(last_element, "-zc") == 0){
        chosen_option = last_element;
    }
}

// Runtime data is always char/string, convert it to proper data
pid_t type_cast_input_to_pid(char *provided_id){
    char *ptr;
    long int process_d = strtol(provided_id, &ptr, 10);
    if (*ptr != '\0' || provided_id <= 0) {
        // Not valid
        return -1;
    }
    return (pid_t)process_d;
}

// This method fills the necessary data
void populate_parr_with_provided_ids(int *argc, char *argv[]){
    if(chosen_option == NULL){
        provided_process_ids = *argc - 2;
    }
    else{
        provided_process_ids = *argc - 3;
    }
    for(int i = 0; i < provided_process_ids; i++){
        // Are Ids okay:
        long int id = type_cast_input_to_pid(argv[i+2]);
        if(id != -1){
            provided_process_ids_arr[i] = id;
        }
        else{
            fprintf(stderr, "%s is not a valid ID.\n", argv[i+2]);
            exit(1);
        }
    }
} 

// Verify and return true or false depending on the args
bool check_arguments(int *argc, char *argv[]){
    if (*argc < 2 || *argc > 8){
        fprintf(stderr, "Arguments are not valid. Please check.\n");
        print_synopsis();
        return false;
    }

    // opt is always provided in the last arg. Check the last arg for option
    char *last_element = argv[*argc-1];
    check_and_assign_last_element(last_element);

    // Next logic verifies the num of args based on provided opt
    if((chosen_option == NULL && *argc < 3) || (chosen_option == NULL && *argc > 7)){
        fprintf(stderr, "Arguments are not valid. Please check.\n");
        print_synopsis();
        return false;
    }
    else if((chosen_option != NULL && *argc < 3) || (chosen_option != NULL && *argc > 8)){
        fprintf(stderr, "Arguments are not valid. Please check.\n");
        print_synopsis();
        return false;
    }

    // no return statement encountered, args are okay, populate required data
    populate_parr_with_provided_ids(argc, argv);
    return true;
}

void get_process_tree(char *argv[]){
    // Declare chr arr
    char cli[50];
    // extract identification of the ps to get tree
    int id_of_root_process = atoi(argv[1]);
    // command in next line is used to make an str with dynamic var in it
    sprintf(cli, "pstree -p %d > tree.txt", id_of_root_process);
    // sys is used to run cmnd which was built
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to get the process tree.\n");
        exit(1);
    }
}

// funct that returns findings of either of '('/ ')'
char* getBracket(int value, char *ln) {
    if (value == 1) {
        return strchr(ln, '(');
    } else if (value == 2) {
        return strchr(ln, ')');
    }
}

void read_output_file(){
    // Access data placed in txt fl
    FILE *process_tree_fl = fopen("tree.txt", "r");
    if (process_tree_fl == NULL) {
        fprintf(stderr, "Failed to open the file");
        exit(1);
    }
    char ln[1001];  // init chr arr for placing read rows from txt file
    // need to check every row of the txt fl
    while (fgets(ln, sizeof(ln), process_tree_fl) != NULL) {
        char* leftBrk;
        char* rtBrk;
        int processId;
        char* line = ln;
        // our identities are stored inside '()', so we check for them in logic ahead
        while ((leftBrk = getBracket(1, line)) != NULL && (rtBrk = getBracket(2, line)) != NULL) {
             // + 1 means next element, that is after '('
            processId = atoi(leftBrk + 1);
            // if found fill the arr
            if (processId != 0) {
                process_id_arr[count_of_prc_in_file] = processId;
                count_of_prc_in_file++;
            }
            // increase ptr for further PSs
            line = rtBrk + 1; 
        }
    }
    // Built info, no need for fl anymore
    fclose(process_tree_fl);
    remove("tree.txt");
}

// funct for checking if ps is there in tree. 0 = Not there, 1 = Present
int is_id_presnt(int given_id){
    int flg = 0;
    for(int j = 0; j < count_of_prc_in_file; j++){
        if(process_id_arr[j] == given_id){
            flg = 1;
        }
    }
    return flg;
}

void print_id_ppid(int id){
    // ini chr arr 
    char cli[50];
    // used for structuring strs for dynamic info
    sprintf(cli, "ps -o ppid= -p %d > ids.txt", id);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to get the ppid of %d.\n", id);
        exit(1);
    }
    int descriptor_fl = open("ids.txt", O_RDONLY);
    if(descriptor_fl == -1){
        fprintf(stderr, "Faild to open the file.\n");
    }
    char *read_ps_ds[100];
    long int read_d = read(descriptor_fl, read_ps_ds, 100);
    read_ps_ds[read_d] = '\0';
    printf("PID: %d and PPID: %s\n", id, read_ps_ds);
    close(descriptor_fl);
    remove("ids.txt");
}

// For each valid id, this will depict data
void check_and_print_ID_if_belong_to_tree(){
    for(int i = 0; i < provided_process_ids; i++){
        int id_is_there = is_id_presnt(provided_process_ids_arr[i]);
        if(id_is_there == 0){
            printf("\nID %d is not present in the process tree.\n", provided_process_ids_arr[i]);
        }
        else{
            printf("\nFor process: %d\n", provided_process_ids_arr[i]);
            print_id_ppid(provided_process_ids_arr[i]);
        }
    }
}

void recursive_fun_nd(int id, bool for_grd_chld){
    // init chr arr
    char cli[50];
    // Dynamically fill chr arr with provided info
    sprintf(cli, "ps -o pid --no-headers --ppid %d > non-direct-descendants.txt", id);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to run the command.\n");
        exit(1);
    }
    FILE *id_f = fopen("non-direct-descendants.txt", "r");
    if(id_f == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    // Vars to handle required logic
    char ln[10000];
    int co = 0;
    // read rows
    while(fgets(ln, sizeof(ln), id_f) != NULL){
        int curre_id = atoi(ln);
        non_direct_descendants_arr[count_for_nds] = curre_id;
        co = co + 1;
        count_for_nds = count_for_nds + 1;
        if(for_grd_chld){
            grd_chld_arr[no_grd_chld] = curre_id;
            no_grd_chld = no_grd_chld + 1;
        }
    }
    fclose(id_f);
    remove("non-direct-descendants.txt");
    if(co != 0){
        for(int i = count_for_nds - co; i < count_for_nds; i++){
            // call exact funct again
            recursive_fun_nd(non_direct_descendants_arr[i], false);
        }
    }
}

void get_descendants(){
    // initialize chr arr
    char cli[50];
    // populate dynamic data inside chr arr
    sprintf(cli, "ps -o pid,state --no-headers --ppid %d > direct-descendants.txt", provided_process_ids_arr[0]);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to run the command.\n");
        exit(1);
    }
    FILE *id_f = fopen("direct-descendants.txt", "r");
    if(id_f == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    int ids[1000];
    int id_nos = 0;
    char ln[10000];
    // data is filled in a txt fl, read new row and fill info
    while(fgets(ln, sizeof(ln), id_f) != NULL){
        int curre_id;
        char status[15];
        // get info and fill exact data type
        sscanf(ln, "%d %s", &curre_id, status);
        if(strcmp(status, "Z") == 0){
            // Z stands for def processes
            dd_zz_arr[no_dd_zz] = curre_id;
            no_dd_zz = no_dd_zz + 1;
        }
        // not def, fill different arr for info
        ids[id_nos] = curre_id;
        id_nos = id_nos + 1;
    }
    fclose(id_f);
    remove("direct-descendants.txt");
    bool for_grd_chld = true;
    // as of now got the Direct child, for child of child, call for iteration, with flgs
    for(int i = 0; i < id_nos; i++){
        recursive_fun_nd(ids[i], for_grd_chld);
        direct_descendants_arr[count_for_ds] = ids[i];
        count_for_ds = count_for_ds + 1;
    }
}

void print_non_direct_descendants(){
    printf("Non direct descendants are:\n");
    for(int i = 0; i < count_for_nds; i++){
        printf("%d ",non_direct_descendants_arr[i]);
    }
    printf("\n");
}

void print_direct_descendants(){
    printf("Direct descendants are:\n");
    for(int i = 0; i < count_for_ds; i++){
        printf("%d ",direct_descendants_arr[i]);
    }
    printf("\n");
}

void get_siblings(){
    // declare chr arr
    char cli[50];
    // populate chr arr using dynamic root id
    sprintf(cli, "ps -o pid,state --no-headers --ppid $(ps -o ppid= -p %d) > siblings.txt", provided_process_ids_arr[0]);
    int command_executed = system(cli);
    if(command_executed == -1){
        fprintf(stderr, "Failed to run the command.\n");
        exit(1);
    }
    FILE *id_f = fopen("siblings.txt", "r");
    if(id_f == NULL){
        fprintf(stderr, "Failed to open the file.\n");
        exit(1);
    }
    char ln[10000];
    // info stored in txt, arr of type ch (ln) is needed to read info
    while(fgets(ln, sizeof(ln), id_f) != NULL){
        int curre_id;
        char status[15];
        // transform chr data into valid type 
        sscanf(ln, "%d %s", &curre_id, status);
        if(strcmp(status, "Z") == 0){
            // Z is for Def PSs
            if(curre_id == provided_process_ids_arr[0]){
                id1_is_zom = true;
            }
            zom_sibs[no_zom_sibs] = curre_id;
            no_zom_sibs = no_zom_sibs + 1;
        }
        // Sib not a def PS
        sib_ids[no_sib_ids] = curre_id;
        no_sib_ids= no_sib_ids + 1;
    }
    fclose(id_f);
    remove("siblings.txt");
}

void print_siblings(){
    printf("Siblings are: \n");    
    for(int i = 0; i < no_sib_ids; i++){
        printf("%d ", sib_ids[i]);
    }
    printf("\n");
}

void print_zom_sibs(){
    printf("Defunct siblings are: \n");    
    for(int i = 0; i < no_zom_sibs; i++){
        printf("%d ", zom_sibs[i]);
    }
    printf("\n");
}

void print_grd_chld(){
    printf("Grand Children are:\n");
    for(int i = 0; i < no_grd_chld; i++){
        printf("%d ", grd_chld_arr[i]);
    }
    printf("\n");
}

void print_status_id1(){
    if(id1_is_zom){
        printf("DEFUNCT\n");
    }
    else{
        printf("NOT DEFUNCT\n");
    }
}

void print_dd_zz(){
    printf("Direct descendants in defunct state are:\n");
    for(int i = 0; i < no_dd_zz; i++){
        printf("%d ", dd_zz_arr[i]);
    }
    printf("\n");
}

void print_for_option(){
    if(strcmp(chosen_option, "-nd") == 0){
        get_descendants();
        print_non_direct_descendants();
    }
    else if(strcmp(chosen_option, "-dd") == 0){
        get_descendants();
        print_direct_descendants();
    }
    else if(strcmp(chosen_option, "-sb") == 0){
        get_siblings();
        print_siblings();
    }
    else if(strcmp(chosen_option, "-sz") == 0){
        get_siblings();
        print_zom_sibs();
    }
    else if(strcmp(chosen_option, "-gc") == 0){
        get_descendants();
        print_grd_chld();
    }
    else if(strcmp(chosen_option, "-zz") == 0){
        get_siblings();
        print_status_id1();
    }
    else if(strcmp(chosen_option, "-zc") == 0){
        get_descendants();
        print_dd_zz();
    }
}

void print_output(char *argv[]){
    read_output_file();
    check_and_print_ID_if_belong_to_tree();
    if(chosen_option != NULL){
        print_for_option();
    }
}

// Program execution starts from here
int main(int argc, char *argv[]){
    // Verify provided info is valid or not
    bool are_arguments_valid = check_arguments(&argc, argv);
    if(!are_arguments_valid){
        exit(1);
    }

    // Arguments are valid, next step is to get ps tree branching from root 
    get_process_tree(argv);

    // Got tree, print output
    print_output(argv);
    return 0;
}